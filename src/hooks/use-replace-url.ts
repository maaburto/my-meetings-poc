import { useEffect } from "react";
import { useHistory } from "react-router-dom";

interface UseReplaceUrlParams {
  hasToRedirect: boolean;
  url: string;
}

const useRouting = (params?: UseReplaceUrlParams) => {
  const { replace, push } = useHistory();

  useEffect(() => {
    if (params?.hasToRedirect && params?.url) {
      replace(params.url);
    }
  }, [params?.hasToRedirect, params?.url]);

  return { replace, push };
};

export default useRouting;
