import { createContext, FC, useState } from "react";
import { ChildrenReactNode } from "../models/general";
import { Meetup } from "../models/meetup";

interface IFavoritesContext {
  favorites: Meetup[];
  totalFavorites: number;
  addFavorite: (favoriteMeetup: Meetup) => void;
  removeFavorite: (meetupId: string) => void;
  itemIsFavorite: (meetupId: string) => boolean;
}

export const FavoritesContext = createContext<IFavoritesContext>({
  favorites: [],
  totalFavorites: 0,
  addFavorite: () => {},
  removeFavorite: () => {},
  itemIsFavorite: () => {
    return false;
  },
});

const FavoritesContextProvider: FC<{ children: ChildrenReactNode }> = (
  props
) => {
  const [userFavorites, setUserFavorites] = useState<Meetup[]>([]);

  const addFavoiteHandler = (favoriteMeetup: Meetup) => {
    setUserFavorites((userFavoritesPrev) =>
      userFavoritesPrev.concat(favoriteMeetup)
    );
  };

  const removeFavoiteHandler = (meetupId: string) => {
    setUserFavorites((preUserFavorites) =>
      preUserFavorites.filter((favorite) => favorite.id !== meetupId)
    );
  };

  const itemIsFavoriteHandler = (meetupId: string) => {
    return userFavorites.some((favorite) => favorite.id === meetupId);
  };

  const context: IFavoritesContext = {
    favorites: userFavorites,
    totalFavorites: userFavorites.length,
    addFavorite: addFavoiteHandler,
    removeFavorite: removeFavoiteHandler,
    itemIsFavorite: itemIsFavoriteHandler,
  };

  return (
    <FavoritesContext.Provider value={context}>
      {props.children}
    </FavoritesContext.Provider>
  );
};

export default FavoritesContextProvider;
