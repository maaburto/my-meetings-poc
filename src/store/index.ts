import { configureStore } from "@reduxjs/toolkit";
import FavoriteMeetups from "./favorites-slice";

const store = configureStore({
  reducer: {
    [FavoriteMeetups.name]: FavoriteMeetups.reducer,
  },
});
export type RootState = ReturnType<typeof store.getState>;

export default store;
