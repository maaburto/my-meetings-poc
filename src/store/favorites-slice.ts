import { createSlice, CaseReducer, PayloadAction } from "@reduxjs/toolkit";
import { Meetup } from "../models/meetup";

interface IFavoriteMeetups {
  favorites: Meetup[];
  favoritesRecord: Record<string, Meetup>;
  totalFavorites: number;
}

const initialState: IFavoriteMeetups = {
  favorites: [],
  totalFavorites: 0,
  favoritesRecord: {},
};

const addFavorite: CaseReducer<IFavoriteMeetups, PayloadAction<Meetup>> = (
  state,
  action
) => {
  state.favorites.push(action.payload);
  state.favoritesRecord[action.payload.id] = action.payload;
  state.totalFavorites++;
};

const removeFavorite: CaseReducer<IFavoriteMeetups, PayloadAction<string>> = (
  state,
  action
) => {
  state.favorites = state.favorites.filter(
    (favorite) => favorite.id !== action.payload
  );
  delete state.favoritesRecord[action.payload];
  state.totalFavorites--;
};

const FavoriteMeetups = createSlice({
  name: "favoriteMeetups",
  initialState,
  reducers: {
    addFavorite,
    removeFavorite,
  },
});

export const favoriteMeetupsActions = FavoriteMeetups.actions;

export default FavoriteMeetups;
