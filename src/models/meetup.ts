export interface Meetup {
  id: string;
  title: string;
  image: string;
  address: string;
  description: string;
}

export type MeetupRecord = Record<string, Omit<Meetup, "id">>;
