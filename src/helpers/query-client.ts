import { QueryClient } from "react-query";

export const queryClient = new QueryClient();

export const MEETUP_LIST = "meetup-list";
export const MEETUP_DETAIL = "meetup-detail";
