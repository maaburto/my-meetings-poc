import { SortingBasicCompareFn } from "../models/general";

export const sortingBasicCompare: SortingBasicCompareFn = (a, b, isAsc) => {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
};
