export const secondsToMilliseconds = (seconds: number) => seconds * 1000;
