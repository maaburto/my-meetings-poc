export enum HTTP_ACTIONS {
  SEND_REQUEST = 'SEND_REQUEST',
  RESPONSE = 'RESPONSE',
  ERROR = 'ERROR',
  CLEAR = 'CLEAR',
  CLEAR_MESSAGE_ERROR = 'CLEAR_MESSAGE_ERROR',
}

export interface HyperTextTransferProtocolState<D = any, E = any> {
  data: D | null;
  error: string | null;
  extra: E | null;
  loading: boolean;
}

type Action<D, E> =
  | { type: HTTP_ACTIONS.SEND_REQUEST }
  | { type: HTTP_ACTIONS.RESPONSE; data?: D; extra?: E }
  | { type: HTTP_ACTIONS.ERROR; message: string }
  | { type: HTTP_ACTIONS.CLEAR }
  | { type: HTTP_ACTIONS.CLEAR_MESSAGE_ERROR };

export type HttpReducerFn<D = any, E = any> = (
  state: HyperTextTransferProtocolState<D, E>,
  action: Action<D, E>
) => HyperTextTransferProtocolState<D, E>;

const hyperTextTransferProtocolReducer: HttpReducerFn = (httpState, action) => {
  switch (action.type) {
    case HTTP_ACTIONS.SEND_REQUEST: {
      return {
        ...httpState,
        error: null,
        extra: null,
        loading: true,
      };
    }

    case HTTP_ACTIONS.RESPONSE: {
      return {
        ...httpState,
        data: action.data,
        extra: action.extra,
        loading: false,
      };
    }

    case HTTP_ACTIONS.ERROR: {
      return {
        ...httpState,
        error: action.message,
        loading: false,
      };
    }

    case HTTP_ACTIONS.CLEAR: {
      return {
        loading: false,
        error: null,
        data: null,
        extra: null,
      };
    }

    case HTTP_ACTIONS.CLEAR_MESSAGE_ERROR: {
      return {
        ...httpState,
        error: null,
      };
    }

    default:
      throw new Error('Should not get there!');
  }
};

export default hyperTextTransferProtocolReducer;
