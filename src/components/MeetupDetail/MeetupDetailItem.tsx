import { FC } from "react";
import classes from "./MeetupDetail.module.css";
import { Meetup } from "../../models/meetup";

type ItemDetailed = Omit<Meetup, "id">;
export interface MeetupDetailItemProps extends Partial<ItemDetailed> {}

const MeetupDetailItem: FC<MeetupDetailItemProps> = (props) => {
  return (
    <>
      <div className={classes.image}>
        <img src={props?.image} alt={props?.title} />
      </div>

      <div className={classes.content}>
        <address>{props?.address}</address>
      </div>

      <div className={classes.content}>
        <p>{props?.description}</p>
      </div>
    </>
  );
};

export default MeetupDetailItem;
