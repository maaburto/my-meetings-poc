import { ChangeEvent, useEffect, useMemo, useState } from "react";

function slowFn(num: number) {
  console.log("Calling it Slow Function");
  for (let i = 0; i < 1000000000; i++) {}

  return num * 2;
}

const ExampleUseMemo = () => {
  const [number, setNumber] = useState(0);
  const [dark, setDark] = useState(false);
  // Heavy calculation for useMemo
  const doubleNumber = useMemo(() => slowFn(number), [number]);

  // Equality Reference for useMemo
  const themeStyles = useMemo(
    () => ({
      backgroundColor: dark ? "black" : "white",
      color: dark ? "white" : "black",
    }),
    [dark]
  );

  useEffect(() => {
    console.log("Theme Changed!");
  }, [themeStyles]);

  const changeNumberHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setNumber(parseInt(e.target.value, 10));
  };

  const onChangeThemeHandler = () => {
    setDark((prevDark) => !prevDark);
  };

  return (
    <>
      <h5>{"Use of useMemo Hook"}</h5>
      <input type='number' value={number} onChange={changeNumberHandler} />
      <button onClick={onChangeThemeHandler}>Change Theme</button>
      <div style={themeStyles}>{doubleNumber}</div>
    </>
  );
};

export default ExampleUseMemo;
