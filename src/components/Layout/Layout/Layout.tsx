import { FC } from "react";
import { ChildrenReactNode } from "../../../models/general";
import { MainNavigation } from "../MainNavigation";
import classes from "./Layout.module.css";

interface LayoutProps {
  children: ChildrenReactNode;
}

const Layout: FC<LayoutProps> = (props) => {
  return (
    <div>
      <MainNavigation />
      <main className={classes.main}>{props.children}</main>
    </div>
  );
};

export default Layout;
