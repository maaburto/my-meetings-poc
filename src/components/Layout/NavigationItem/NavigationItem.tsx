import { FC } from "react";
import { Link } from "react-router-dom";
import { NavRoutes } from "../layout.interface";
import classes from "./NavigationItem.module.css";

interface NavigationItemProps extends NavRoutes {
  badgeDescription?: string;
}

const NavigationItem: FC<NavigationItemProps> = (props) => {
  return (
    <li className={classes["item"]}>
      <Link to={props.linkTo}>{props.name}</Link>
      {props.badgeDescription && (
        <span className={classes.badge}>{props.badgeDescription}</span>
      )}
    </li>
  );
};

export default NavigationItem;
