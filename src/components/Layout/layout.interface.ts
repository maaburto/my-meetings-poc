export interface NavRoutes {
  name: string;
  linkTo: string;
  badgeDescription?: string;
}
