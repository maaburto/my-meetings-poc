import { FC } from "react";
import { Meetup } from "../../../models/meetup";
import { MeetupWrapperItem } from "../MeetupWrapperItem";
import classes from "./MeetupList.module.css";

interface MeetupListProps {
  meetups?: Meetup[];
  onToogleFavorite: (meetup: Meetup, isFavorite: boolean) => void;
  onViewMeetup: (meetupId: string) => void;
  isLoading: boolean;
  error: unknown;
  favoritesRecord: Record<string, Meetup>;
}

const MeetupList: FC<MeetupListProps> = (props) => {
  if (props.isLoading) {
    return (
      <div className={classes.loading}>
        <h2>Loading...</h2>
      </div>
    );
  }

  return (
    <ul className={classes.list}>
      {props.meetups?.map((meetup) => (
        <MeetupWrapperItem
          key={meetup.id}
          meetup={meetup}
          favoritesRecord={props.favoritesRecord}
          onToogleFavorite={props.onToogleFavorite}
          onViewItem={props.onViewMeetup}
        />
      ))}
    </ul>
  );
};

export default MeetupList;
