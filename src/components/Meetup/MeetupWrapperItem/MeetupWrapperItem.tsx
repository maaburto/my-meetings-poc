import { FC } from "react";
import { Meetup } from "../../../models/meetup";
import MeetupItem from "../MeetupItem/MeetupItem";

interface MeetupWrapperItemProps {
  meetup: Meetup;
  favoritesRecord: Record<string, Meetup>;
  onToogleFavorite: (meetup: Meetup, isFavorite: boolean) => void;
  onViewItem: (meetupId: string) => void;
}

const MeetupWrapperItem: FC<MeetupWrapperItemProps> = (props) => {
  const isFavorite = !!props.favoritesRecord[props.meetup?.id]?.id;

  return (
    <MeetupItem
      address={props.meetup.address}
      image={props.meetup.image}
      title={props.meetup.title}
      isFavorite={isFavorite}
      onToogleFavorite={props.onToogleFavorite.bind(
        null,
        props.meetup,
        isFavorite
      )}
      onViewItem={props.onViewItem.bind(null, props.meetup.id)}
    />
  );
};

export default MeetupWrapperItem;
