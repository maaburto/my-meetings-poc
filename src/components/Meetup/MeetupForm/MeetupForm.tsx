import { FC, FormEvent, useRef } from "react";
import { Card } from "../../Card";
import { NewMeetUp } from "./MeetupForm.interface";
import classes from "./MeetupForm.module.css";

interface MeetupFormProps {
  isLoading?: boolean;
  onNewMeetup: (value: NewMeetUp) => void;
}

const MeetupForm: FC<MeetupFormProps> = (props) => {
  const titleInputRef = useRef<HTMLInputElement>(null);
  const imageInputRef = useRef<HTMLInputElement>(null);
  const addressInputRef = useRef<HTMLInputElement>(null);
  const descriptionAreaRef = useRef<HTMLTextAreaElement>(null);
  const submitHandler = (event: FormEvent) => {
    event.preventDefault();

    const values: NewMeetUp = {
      title: titleInputRef.current?.value || "",
      image: imageInputRef.current?.value || "",
      address: addressInputRef.current?.value || "",
      description: descriptionAreaRef.current?.value || "",
    };

    props.onNewMeetup(values);
  };

  return (
    <Card>
      <form className={classes.form} onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor='title'>Meetup Title</label>
          <input id='title' type={"text"} required ref={titleInputRef} />
        </div>

        <div className={classes.control}>
          <label htmlFor='image'>Image</label>
          <input id='image' type={"url"} required ref={imageInputRef} />
        </div>

        <div className={classes.control}>
          <label htmlFor='address'>Address</label>
          <input id='address' type={"text"} required ref={addressInputRef} />
        </div>

        <div className={classes.control}>
          <label htmlFor='description'>Description</label>
          <textarea
            id='description'
            rows={5}
            required
            ref={descriptionAreaRef}
          ></textarea>
        </div>

        {!props.isLoading && (
          <div className={classes.actions}>
            <button type='submit'>Add Meetup</button>
          </div>
        )}

        {props.isLoading && <span>Sending Meetup, please wait...</span>}
      </form>
    </Card>
  );
};

export default MeetupForm;
