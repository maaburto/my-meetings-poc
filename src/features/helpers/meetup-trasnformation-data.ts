import { Meetup } from "../../models/meetup";

export function transformToMeetupList(meetup: any): Meetup[] {
  if (!meetup) {
    return [];
  }

  return Object.keys(meetup).reduce<Meetup[]>(
    (allItems, meetupId) => [
      ...allItems,
      {
        id: meetupId,
        ...meetup[meetupId],
      },
    ],
    []
  );
}

export function transformToMeetupRecord(meetup: any): Record<string, Meetup> {
  if (!meetup) {
    return {};
  }

  return Object.keys(meetup).reduce<Record<string, Meetup>>(
    (record, meetupId) => ({
      ...record,
      [meetupId]: {
        id: meetupId,
        ...meetup[meetupId],
      },
    }),
    {}
  );
}
