import { useParams } from "react-router-dom";
import { useFetchOneMeetup } from "../hooks/use-fetch-one-meetup";
import { MeetupDetail } from "../../components/MeetupDetail";
import { useRouting } from "../../hooks";

const MeetupDetailedWrapper = () => {
  const params = useParams<{ meetId: string }>();
  const { meetup, isSuccess, isLoading } = useFetchOneMeetup(params.meetId);
  useRouting({
    hasToRedirect: !meetup && isSuccess,
    url: "/",
  });

  return (
    <div>
      {meetup && <h1>{meetup.title}</h1>}
      <MeetupDetail
        address={meetup?.address}
        description={meetup?.description}
        image={meetup?.image}
        title={meetup?.title}
        loading={isLoading}
      />
    </div>
  );
};

export default MeetupDetailedWrapper;
