import { useMutation } from "react-query";
import { createMeetup } from "../services/meetups";

interface useCreateMeetupParams {
  onSuccess?: () => void;
}
export const useCreateMeetup = (params: useCreateMeetupParams) => {
  const { onSuccess } = params;
  const { isLoading, data, isSuccess, error, mutate } = useMutation(
    createMeetup,
    {
      onSuccess,
    }
  );

  return { isLoading, data, isSuccess, error, mutate };
};
