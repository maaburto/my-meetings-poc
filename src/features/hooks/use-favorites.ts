import { useDispatch, useSelector } from "react-redux";
import { useRouting } from "../../hooks";
import { Meetup } from "../../models/meetup";
import { RootState } from "../../store";
import { favoriteMeetupsActions } from "../../store/favorites-slice";

export const useFavorites = () => {
  const { push } = useRouting();
  const dispatch = useDispatch();
  const favorites = useSelector(
    (state: RootState) => state.favoriteMeetups.favorites
  );
  const favoritesRecord = useSelector(
    (state: RootState) => state.favoriteMeetups.favoritesRecord
  );

  const toogleFavorite = (meetup: Meetup, isFavorite: boolean) => {
    if (!isFavorite) {
      dispatch(favoriteMeetupsActions.addFavorite(meetup));
    } else {
      dispatch(favoriteMeetupsActions.removeFavorite(meetup.id));
    }
  };

  const goMeetup = (meetupId: string) => {
    push(`/meet/${meetupId}`);
  };

  return { toogleFavorite, favorites, favoritesRecord, goMeetup };
};
