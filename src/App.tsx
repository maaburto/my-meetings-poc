import { Route, Switch as SwithRouter } from "react-router-dom";

import {
  AllMeetupsPage,
  NewMeetupPage,
  FavoritesPage,
  MeetupDetailPage,
} from "./pages";
import { Layout } from "./components/Layout";
import "./App.css";

function App() {
  return (
    <Layout>
      <SwithRouter>
        <Route path='/' exact>
          <AllMeetupsPage />
        </Route>

        <Route path='/new-meetup'>
          <NewMeetupPage />
        </Route>

        <Route path='/favorites'>
          <FavoritesPage />
        </Route>

        <Route path='/meet/:meetId'>
          <MeetupDetailPage />
        </Route>
      </SwithRouter>
    </Layout>
  );
}

export default App;
