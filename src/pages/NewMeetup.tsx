import { useHistory } from "react-router-dom";
import { MeetupForm, NewMeetUp } from "../components/Meetup";
import { useCreateMeetup } from "../features/hooks/use-create-meetup";
import { ReactQueryDevtools } from "react-query/devtools";
import { MEETUP_LIST, queryClient } from "../helpers/query-client";

const NewMeetupPage = () => {
  const { isLoading, isSuccess, mutate } = useCreateMeetup({
    onSuccess: () => {
      queryClient.invalidateQueries(MEETUP_LIST);
    },
  });
  const history = useHistory();

  const newMeetupHandler = (value: NewMeetUp) => {
    mutate(value);
  };

  if (!isLoading && isSuccess) {
    history.replace("/");
  }

  return (
    <section>
      <h1>Add New Meetup</h1>
      <MeetupForm isLoading={isLoading} onNewMeetup={newMeetupHandler} />
      <ReactQueryDevtools />
    </section>
  );
};

export default NewMeetupPage;
