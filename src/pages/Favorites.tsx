import { FC } from "react";
import { MeetupList } from "../components/Meetup";
import { useFavorites } from "../features";

const FavoritesMeetupPage: FC = () => {
  const { favorites, favoritesRecord, toogleFavorite, goMeetup } =
    useFavorites();

  return (
    <section>
      <h1>Favorites</h1>
      <MeetupList
        meetups={favorites}
        error={null}
        isLoading={false}
        favoritesRecord={favoritesRecord}
        onToogleFavorite={toogleFavorite}
        onViewMeetup={goMeetup}
      />
    </section>
  );
};

export default FavoritesMeetupPage;
