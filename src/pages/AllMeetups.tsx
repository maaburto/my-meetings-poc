import { FC } from "react";
import { MeetupList } from "../components/Meetup";
import { useFavorites, useFetchMeetups } from "../features";
import { ReactQueryDevtools } from "react-query/devtools";

const AllMeetupsPage: FC = () => {
  const { meetups, error, isLoading, isFetching } = useFetchMeetups({
    generateRecord: true,
  });
  const { favoritesRecord, toogleFavorite, goMeetup } = useFavorites();

  return (
    <section>
      <h1>All Meetups</h1>

      <MeetupList
        meetups={meetups}
        error={error}
        isLoading={isLoading || isFetching}
        favoritesRecord={favoritesRecord}
        onToogleFavorite={toogleFavorite}
        onViewMeetup={goMeetup}
      />
      <ReactQueryDevtools />
    </section>
  );
};

export default AllMeetupsPage;
